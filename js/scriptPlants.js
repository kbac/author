onload = function() {
    var one = document.getElementsByClassName('plants_title');
    var two = document.getElementsByClassName('plants_button');
    var f = document.getElementsByTagName("form")[0];

    function classToggle(evt) {
        [].forEach.call(two, function (a, i) {
            if (a == evt.target) {
                a.classList[evt.type === 'mouseover' ? 'add' : 'remove']('plants_ch');
                one[i].classList[evt.type === 'mouseover' ? 'add' : 'remove']('plants_ch');
            }
        });
    }

    for (var i = 0, len = two.length; i < len; i++) {
        two[i].addEventListener('mouseover', function (e) {
            classToggle(e);
        });
        two[i].addEventListener('mouseout', function (e) {
            classToggle(e);
        });
    }



}



