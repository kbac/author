$(document).ready(function ($) {
    $('.bntsub').click(function(){
        var error = false;

        var name = $('input#name').val();
        if(name == "" || name == " ") {
            $('#error-name').fadeIn('slow');
            error = true;
        }else{
            $('#error-name').fadeOut('slow');
        }

        var email_compare = /^([a-z0-9_.-]+)@([da-z.-]+).([a-z.]{2,6})$/;
        var email = $('input#emaill').val();
        if (email == "" || email == " ") {
            $('#error-email').fadeIn('slow');
            error = true;
        }else if (!email_compare.test(email)) {
            $('#error-email').fadeIn('slow');
            error = true;
        }else{
            $('#error-email').fadeOut('slow');
        }

        var message = $('textarea#message').val();
        if(message == "" || message == " " ) {
            $('#error-sms').fadeIn('slow');
            error = true;
        }else if (message.length <= 2) {
            $('#error-sms').fadeIn('slow');
            error = true;
        }else{
            $('#error-sms').fadeOut('slow');
        }
        return false;
    });
});

